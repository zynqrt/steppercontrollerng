# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  set C_S00_AXI_DATA_WIDTH [ipgui::add_param $IPINST -name "C_S00_AXI_DATA_WIDTH" -parent ${Page_0} -widget comboBox]
  set_property tooltip {Width of S_AXI data bus} ${C_S00_AXI_DATA_WIDTH}
  set C_S00_AXI_ADDR_WIDTH [ipgui::add_param $IPINST -name "C_S00_AXI_ADDR_WIDTH" -parent ${Page_0}]
  set_property tooltip {Width of S_AXI address bus} ${C_S00_AXI_ADDR_WIDTH}
  ipgui::add_param $IPINST -name "C_S00_AXI_BASEADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_S00_AXI_HIGHADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_SPEED_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_POSITION_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_PULSE_TIME_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_PULSE_TIME" -parent ${Page_0}
  ipgui::add_param $IPINST -name "C_STEPS_PER_LINE_WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.C_POSITION_WIDTH { PARAM_VALUE.C_POSITION_WIDTH } {
	# Procedure called to update C_POSITION_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_POSITION_WIDTH { PARAM_VALUE.C_POSITION_WIDTH } {
	# Procedure called to validate C_POSITION_WIDTH
	return true
}

proc update_PARAM_VALUE.C_PULSE_TIME { PARAM_VALUE.C_PULSE_TIME } {
	# Procedure called to update C_PULSE_TIME when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_PULSE_TIME { PARAM_VALUE.C_PULSE_TIME } {
	# Procedure called to validate C_PULSE_TIME
	return true
}

proc update_PARAM_VALUE.C_PULSE_TIME_WIDTH { PARAM_VALUE.C_PULSE_TIME_WIDTH } {
	# Procedure called to update C_PULSE_TIME_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_PULSE_TIME_WIDTH { PARAM_VALUE.C_PULSE_TIME_WIDTH } {
	# Procedure called to validate C_PULSE_TIME_WIDTH
	return true
}

proc update_PARAM_VALUE.C_SPEED_WIDTH { PARAM_VALUE.C_SPEED_WIDTH } {
	# Procedure called to update C_SPEED_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_SPEED_WIDTH { PARAM_VALUE.C_SPEED_WIDTH } {
	# Procedure called to validate C_SPEED_WIDTH
	return true
}

proc update_PARAM_VALUE.C_STEPS_PER_LINE_WIDTH { PARAM_VALUE.C_STEPS_PER_LINE_WIDTH } {
	# Procedure called to update C_STEPS_PER_LINE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_STEPS_PER_LINE_WIDTH { PARAM_VALUE.C_STEPS_PER_LINE_WIDTH } {
	# Procedure called to validate C_STEPS_PER_LINE_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to update C_S00_AXI_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_DATA_WIDTH { PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to validate C_S00_AXI_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to update C_S00_AXI_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_ADDR_WIDTH { PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to validate C_S00_AXI_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_BASEADDR { PARAM_VALUE.C_S00_AXI_BASEADDR } {
	# Procedure called to update C_S00_AXI_BASEADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_BASEADDR { PARAM_VALUE.C_S00_AXI_BASEADDR } {
	# Procedure called to validate C_S00_AXI_BASEADDR
	return true
}

proc update_PARAM_VALUE.C_S00_AXI_HIGHADDR { PARAM_VALUE.C_S00_AXI_HIGHADDR } {
	# Procedure called to update C_S00_AXI_HIGHADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.C_S00_AXI_HIGHADDR { PARAM_VALUE.C_S00_AXI_HIGHADDR } {
	# Procedure called to validate C_S00_AXI_HIGHADDR
	return true
}


proc update_MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH { MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH PARAM_VALUE.C_S00_AXI_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_DATA_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_DATA_WIDTH}
}

proc update_MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH { MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH PARAM_VALUE.C_S00_AXI_ADDR_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_S00_AXI_ADDR_WIDTH}] ${MODELPARAM_VALUE.C_S00_AXI_ADDR_WIDTH}
}

proc update_MODELPARAM_VALUE.C_PULSE_TIME { MODELPARAM_VALUE.C_PULSE_TIME PARAM_VALUE.C_PULSE_TIME } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_PULSE_TIME}] ${MODELPARAM_VALUE.C_PULSE_TIME}
}

proc update_MODELPARAM_VALUE.C_PULSE_TIME_WIDTH { MODELPARAM_VALUE.C_PULSE_TIME_WIDTH PARAM_VALUE.C_PULSE_TIME_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_PULSE_TIME_WIDTH}] ${MODELPARAM_VALUE.C_PULSE_TIME_WIDTH}
}

proc update_MODELPARAM_VALUE.C_SPEED_WIDTH { MODELPARAM_VALUE.C_SPEED_WIDTH PARAM_VALUE.C_SPEED_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_SPEED_WIDTH}] ${MODELPARAM_VALUE.C_SPEED_WIDTH}
}

proc update_MODELPARAM_VALUE.C_POSITION_WIDTH { MODELPARAM_VALUE.C_POSITION_WIDTH PARAM_VALUE.C_POSITION_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_POSITION_WIDTH}] ${MODELPARAM_VALUE.C_POSITION_WIDTH}
}

proc update_MODELPARAM_VALUE.C_STEPS_PER_LINE_WIDTH { MODELPARAM_VALUE.C_STEPS_PER_LINE_WIDTH PARAM_VALUE.C_STEPS_PER_LINE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.C_STEPS_PER_LINE_WIDTH}] ${MODELPARAM_VALUE.C_STEPS_PER_LINE_WIDTH}
}

