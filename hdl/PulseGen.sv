`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.10.2020 11:30:34
// Design Name: 
// Module Name: PulseGen
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PulseGen #(
        parameter integer C_PULSE_TIME_WIDTH = 10
    )
    (
        input wire aclk,
        input wire aresetn,
        input wire pulse,
        input wire [C_PULSE_TIME_WIDTH-1:0] pulse_time,
        output reg pulse_rise,
        output reg pulse_out
    );


bit dff;
    always_ff @(posedge aclk)
        if (!aresetn)
            dff <= '0;
        else
            dff <= pulse;

wire pulse_rise_w = pulse & ~dff;
    always_ff @(posedge aclk)
        pulse_rise <= pulse_rise_w;


bit [C_PULSE_TIME_WIDTH-1:0] cnt;
    always_ff @(posedge aclk)
        if (!aresetn)
            cnt <= '0;
        else begin
            if (pulse_rise_w) 
                cnt <= pulse_time;
            else if (cnt > 0)
                cnt <= cnt - 1'b1;
        end

    always_ff @(posedge aclk)
        pulse_out <= (cnt != '0);
        
endmodule
