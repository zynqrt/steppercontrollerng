`ifndef ASSIGN_SVH_
`define ASSIGN_SVH_


`define ASSIGN_AXI_W(register, awaddr, wren, data)	                 \
	assign register.axi_data = data[register.REG_WIDTH-1:0]; 		 \
	assign register.axi_wr = wren & (awaddr == register.REG_ADDRESS); 


`define AXI_REG_ALWAYS(clk, nrst, reg_if, reg_dff) 	\
	bit [reg_if.REG_WIDTH-1:0] reg_dff;             \
	always_ff @(posedge clk)                        \
		if (nrst == 1'b0)                           \
			reg_dff <= '0;                          \
		else if (reg_if.axi_wr)                     \
			reg_dff <= reg_if.axi_data;             \
	assign reg_if.data = reg_dff;

`endif
