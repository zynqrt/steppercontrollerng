`timescale 1ns / 1ps
`include "typedef.svh"
`include "assign.svh"

//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.10.2020 09:22:28
// Design Name: 
// Module Name: StepperDriver
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module StepperDriver #(
		parameter integer C_POSITION_WIDTH = 64,
		parameter integer C_SPEED_WIDTH = 40,
        parameter integer C_PULSE_TIME = 200,
        parameter integer C_FULL_STEP_CNT_WIDTH = 32,
        parameter integer C_STEPS_PER_LINE_WIDTH = 32,
        parameter integer C_ACCELERATION_WIDTH = 32,
        parameter integer C_TIME_WIDTH = 32,
        parameter integer C_PULSE_TIME_WIDTH = 32
	)
	(
		input wire aclk,
		input wire aresetn,

		output wire [C_FULL_STEP_CNT_WIDTH-1:0] FULL_STEP_COUNTER,

        output wire BUSY,
        output wire STEP,
        output wire DIR,
        output wire SCAN_LINE,
        output wire PRINT_LINE,

        output wire st_acc,
        output wire st_dec,
        output wire st_const,

		RegAXI_if.slave reg0_accel_deccel_if,
        RegAXI_if.slave reg1_accel_time_if,
        RegAXI_if.slave reg2_const_time_if,
        RegAXI_if.slave reg3_deccel_time_if,
        RegAXI_if.slave reg4_stepdir_if,
        RegAXI_if.slave reg5_cis_steps_if,
        RegAXI_if.slave reg6_control_if,
        RegAXI_if.slave reg8_manual_mode_if,
        RegAXI_if.slave reg9_manual_control_if
    );

	wire start = reg6_control_if.axi_data[0] & reg6_control_if.axi_wr;
	wire abort = reg6_control_if.axi_data[1] & reg6_control_if.axi_wr;
    assign reg6_control_if.data = '0;

// Регистры
	`AXI_REG_ALWAYS(aclk, aresetn, reg0_accel_deccel_if, DECELERATION_ACCELERATION);
	`AXI_REG_ALWAYS(aclk, aresetn, reg1_accel_time_if, TIME_ACCELERATION);
	`AXI_REG_ALWAYS(aclk, aresetn, reg2_const_time_if, TIME_CONST_SPEED);
	`AXI_REG_ALWAYS(aclk, aresetn, reg3_deccel_time_if, TIME_DECELERATION);
	`AXI_REG_ALWAYS(aclk, aresetn, reg4_stepdir_if, stepdir_reg);
	`AXI_REG_ALWAYS(aclk, aresetn, reg5_cis_steps_if, STEPS_PER_LINE);
	`AXI_REG_ALWAYS(aclk, aresetn, reg8_manual_mode_if, manual_mode_reg);

	wire direction_auto = stepdir_reg[0];
	wire [reg4_stepdir_if.REG_WIDTH-2:0] STEP_SELECTOR = stepdir_reg[reg4_stepdir_if.REG_WIDTH-1:1];

	// Ручной режим
	wire mode_manual = manual_mode_reg[0];
	wire [reg8_manual_mode_if.REG_WIDTH-2:0] manual_pulse_width = manual_mode_reg[reg8_manual_mode_if.REG_WIDTH-1:1];	

bit [C_POSITION_WIDTH-1:0] position_b;
bit [C_SPEED_WIDTH-1:0] speed_b;
bit [C_ACCELERATION_WIDTH-1:0] acceleration_b;
bit [C_ACCELERATION_WIDTH-1:0] deceleration_b;
bit [C_TIME_WIDTH-1:0] time_b;
	
	wire stop_w;

	enum bit [9:0] {
		IDLE,
		LOAD_ACCELERATION,
		COUNTING_ACC,		// Считаем время ускорения
		LOAD_CONST,
		COUNTING_CONST,		// Считаем время в установившемся режиме
		LOAD_DECCELEATION,
		COUNTING_DEC,		// Считаем время замедления
		MANUAL
	} state, next;

	always_ff @(posedge aclk) begin
		if (~aresetn)
			state <= IDLE;
		else
			state <= next;
	end

	always_comb begin : nextStateLogic
		next = state;
		unique case(state)
			IDLE: if (start  && (~mode_manual)) next = LOAD_ACCELERATION;
			LOAD_ACCELERATION: 		next = COUNTING_ACC;
			COUNTING_ACC: begin
				if (mode_manual) next = IDLE;
                if (stop_w) next = LOAD_CONST;
                if (abort) next = IDLE;
            end
            LOAD_CONST: next = COUNTING_CONST;
			COUNTING_CONST: begin
				if (mode_manual) next = IDLE;
                if (stop_w) next = LOAD_DECCELEATION;
                if (abort) next = IDLE;
            end
            LOAD_DECCELEATION: next = COUNTING_DEC;
			COUNTING_DEC: begin
				if (mode_manual) next = IDLE;
                if (stop_w) next = IDLE;
                if (abort) next = IDLE;
            end
		endcase
	end : nextStateLogic


	always_ff @(posedge aclk) begin
		if (~aresetn) begin
			speed_b <= '0;
			acceleration_b <= '0;
			deceleration_b <= '0;
		end
		else begin
			case (next)
				IDLE: begin
					speed_b <= '0;
					acceleration_b <= '0;
					deceleration_b <= '0;
				end

				LOAD_ACCELERATION: begin
					acceleration_b <= DECELERATION_ACCELERATION[C_ACCELERATION_WIDTH-1:0]; // Младшие
					deceleration_b <= DECELERATION_ACCELERATION[C_ACCELERATION_WIDTH*2-1:C_ACCELERATION_WIDTH]; // Старшие
				end

				COUNTING_ACC: begin
					speed_b <= speed_b + acceleration_b;
				end

				COUNTING_CONST: begin
					speed_b <= speed_b;
				end

				COUNTING_DEC: begin
					speed_b <= speed_b - deceleration_b;
				end
			endcase
		end
	end

	// Счетчик положения
	always_ff @(posedge aclk)
		if (!aresetn) begin
			position_b <= '0;
		end
		else begin
			if (next == LOAD_ACCELERATION)
				position_b <= '0;
			else 
				position_b <= position_b + speed_b;
		end

	// Счетчик времени исполнения
	always_ff @(posedge aclk)
		if (!aresetn) begin
			time_b <= '0;
		end
		else begin
			case (next)
				LOAD_ACCELERATION: time_b <= TIME_ACCELERATION;
				COUNTING_ACC: if (time_b > '0) time_b <= time_b - 1'b1;
				LOAD_CONST: time_b <= TIME_CONST_SPEED;
				COUNTING_CONST: if (time_b > '0) time_b <= time_b - 1'b1;
				LOAD_DECCELEATION: time_b <= TIME_DECELERATION;
                COUNTING_DEC: if (time_b > '0) time_b <= time_b - 1'b1;
			endcase
		end

	assign stop_w = (time_b == '0);

    assign BUSY = next != IDLE;
    assign st_acc = (next == COUNTING_ACC);
    assign st_dec = (next == COUNTING_DEC);
    assign st_const = (next == COUNTING_CONST);
    
    wire step_auto;
    wire pulse_rise;
    PulseGen #(
        .C_PULSE_TIME_WIDTH(C_PULSE_TIME_WIDTH)
    ) pg_step (
        .aclk     (aclk),
        .aresetn  (aresetn),
        .pulse_time(C_PULSE_TIME[C_PULSE_TIME_WIDTH-1:0]),
        .pulse    (position_b[STEP_SELECTOR]),
        .pulse_out(step_auto),
        .pulse_rise(pulse_rise)
    );


bit read_line_b;
bit [C_STEPS_PER_LINE_WIDTH-1:0] step_cnt;   
    always_ff @(posedge aclk)
        if (!aresetn) begin
            step_cnt <= '0;
            read_line_b <= '0;
        end
        else begin
            read_line_b <= '0;

            if (pulse_rise)
                step_cnt <= step_cnt + 1'b1;

            if (step_cnt == STEPS_PER_LINE) begin
                step_cnt <= '0;
                read_line_b <= 1'b1;
            end
        end

    wire SCAN_LINE_auto = read_line_b;


// Счетчик шагов полный, верние биты положения
    assign FULL_STEP_COUNTER = position_b[C_POSITION_WIDTH-1:(C_POSITION_WIDTH-C_FULL_STEP_CNT_WIDTH)];

// =================== Ручной режим ===========================================
	wire manual_step_pulse = reg9_manual_control_if.axi_wr & reg9_manual_control_if.axi_data[0];
	wire manual_scanline_pulse = reg9_manual_control_if.axi_wr & reg9_manual_control_if.axi_data[2];
	wire manual_printline_pulse = reg9_manual_control_if.axi_wr & reg9_manual_control_if.axi_data[3];

	bit direction_manual;
	always_ff @(posedge aclk)
		if (aresetn == '0)
			direction_manual <= '0;
		else if (reg9_manual_control_if.axi_wr)
			direction_manual <= reg9_manual_control_if.axi_data[1];
	assign reg9_manual_control_if.data = {1'b0, 1'b0, direction_manual, 1'b0};

    wire step_manual;
    PulseGen #(
    	.C_PULSE_TIME_WIDTH(C_PULSE_TIME_WIDTH)
	) pg_manual (
		.aclk      (aclk),
		.aresetn   (aresetn),
		.pulse     (manual_step_pulse),
		.pulse_time(manual_pulse_width),
		.pulse_out (step_manual)
	);

	assign STEP = (mode_manual == 1'b1) ? step_manual : step_auto;
	assign DIR = (mode_manual == 1'b1) ? direction_manual : direction_auto;

	PulseGen #(
		.C_PULSE_TIME_WIDTH(4)
	) pg_scan_line (
		.aclk      (aclk),
		.aresetn   (aresetn),
		.pulse     ((mode_manual == 1'b1) ? manual_scanline_pulse : SCAN_LINE_auto),
		.pulse_time(4'd15),
		.pulse_out (SCAN_LINE)
	);

	wire PRINT_LINE_manual;
	PulseGen #(
		.C_PULSE_TIME_WIDTH(4)
	) pg_print_line (
		.aclk      (aclk),
		.aresetn   (aresetn),
		.pulse     (manual_printline_pulse),
		.pulse_time(4'd15),
		.pulse_out (PRINT_LINE_manual)
	);

	assign PRINT_LINE = PRINT_LINE_manual;
	
endmodule
