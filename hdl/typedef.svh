`ifndef AXI_TYPEDEF_SVH_
`define AXI_TYPEDEF_SVH_


interface RegAXI_if #(REG_ADDRESS=0, REG_WIDTH=32)(input wire clk);
	logic axi_wr;  					 // строб записи AXI
	logic [REG_WIDTH-1:0] axi_data;  // Данные из AXI
	logic [REG_WIDTH-1:0] data;

	modport master(output axi_wr, output axi_data, input data);
	modport slave(input axi_wr, input axi_data, output data);
endinterface


interface Status_if (input wire clk);
	logic idle;
	logic stb1;
	logic stb2;
	logic wait_printline;

	modport master(output idle, output stb1, output stb2, output wait_printline);
	modport slave (input idle, input stb1, input stb2, input wait_printline);
endinterface


/*
interface RegIrq_if #(REG_WIDTH=32) (input wire clk);
	logic axi_wr_gier;
	logic axi_wr_ipier;
	logic axi_wr_ipisr;
	logic [REG_WIDTH-1:0] axi_data;

	modport master(output axi_wr_gier, output axi_wr_ipier, output axi_wr_ipisr, output axi_data);
	modport slave(input axi_wr_gier, input axi_wr_ipier, input axi_wr_ipisr, input axi_data);
endinterface
*/

`endif
