
`timescale 1 ns / 1 ps
`include "typedef.svh"

	module StepperControllerNg_v1_0 #
	(
		// Users to add parameters here
        parameter integer C_STEPS_PER_LINE_WIDTH = 32,
		parameter integer C_SPEED_WIDTH = 40,
		parameter integer C_POSITION_WIDTH = 64,
		parameter integer C_PULSE_TIME_WIDTH = 10,
        parameter integer C_PULSE_TIME = 200,
		// User parameters ends
		// Do not modify the parameters beyond this line


		// Parameters of Axi Slave Bus Interface S00_AXI
		parameter integer C_S00_AXI_DATA_WIDTH	= 32,
		parameter integer C_S00_AXI_ADDR_WIDTH	= 6
	)
	(
		// Users to add ports here
        output wire STEP,
        output wire DIR,
        output wire BUSY,
        output wire SCAN_LINE,
        output wire PRINT_LINE,
        output wire st_acc,
        output wire st_dec,
        output wire st_const,

		// User ports ends
		// Do not modify the ports beyond this line


		// Ports of Axi Slave Bus Interface S00_AXI
		input wire  s00_axi_aclk,
		input wire  s00_axi_aresetn,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_awaddr,
		input wire [2 : 0] s00_axi_awprot,
		input wire  s00_axi_awvalid,
		output wire  s00_axi_awready,
		input wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_wdata,
		input wire [(C_S00_AXI_DATA_WIDTH/8)-1 : 0] s00_axi_wstrb,
		input wire  s00_axi_wvalid,
		output wire  s00_axi_wready,
		output wire [1 : 0] s00_axi_bresp,
		output wire  s00_axi_bvalid,
		input wire  s00_axi_bready,
		input wire [C_S00_AXI_ADDR_WIDTH-1 : 0] s00_axi_araddr,
		input wire [2 : 0] s00_axi_arprot,
		input wire  s00_axi_arvalid,
		output wire  s00_axi_arready,
		output wire [C_S00_AXI_DATA_WIDTH-1 : 0] s00_axi_rdata,
		output wire [1 : 0] s00_axi_rresp,
		output wire  s00_axi_rvalid,
		input wire  s00_axi_rready
	);
	
	localparam  C_ACCELERATION_WIDTH = 16,
			    C_TIME_WIDTH = C_S00_AXI_DATA_WIDTH,
                C_STEP_SELECTOR_WIDTH = 6;
    
    wire [C_S00_AXI_DATA_WIDTH-1:0] reg7_full_step_cnt;

    RegAXI_if #(
    	.REG_ADDRESS(0),
    	.REG_WIDTH  (C_ACCELERATION_WIDTH*2)
    ) reg0_accel_deccel (
    	.clk(s00_axi_aclk)
    );

    RegAXI_if #(
    	.REG_ADDRESS(1),
    	.REG_WIDTH(C_TIME_WIDTH)
	) reg1_accel_time (
		.clk(s00_axi_aclk)
	);

	RegAXI_if #(
		.REG_ADDRESS(2),
		.REG_WIDTH  (C_TIME_WIDTH)
	) reg2_const_time (
		.clk(s00_axi_aclk)
	);

	RegAXI_if #(
		.REG_ADDRESS(3),
		.REG_WIDTH  (C_TIME_WIDTH)
	) reg3_deccel_time (
		.clk(s00_axi_aclk)
	);

	RegAXI_if #(
		.REG_ADDRESS(4),
		.REG_WIDTH(C_STEP_SELECTOR_WIDTH+1)
	) reg4_stepdir (
		.clk(s00_axi_aclk)
	);

	RegAXI_if #(
		.REG_ADDRESS(5),
		.REG_WIDTH  (C_STEPS_PER_LINE_WIDTH)
	) reg5_cis_steps (
		.clk(s00_axi_aclk)
	);

    RegAXI_if #(
    	.REG_ADDRESS(6),
    	.REG_WIDTH(2)
    ) reg6_control (
    	.clk(s00_axi_aclk)
    );

    RegAXI_if #(
    	.REG_ADDRESS(8),
    	.REG_WIDTH  (C_PULSE_TIME_WIDTH+1) // Регистр длительности импульса и бит режима (ручной(1)/автомат(0)).
	) reg8_manual_mode (
		.clk(s00_axi_aclk)
	);

	// Регистр ручного управления. 4 бита с-мл: print_line-scan_line-dir-step
	RegAXI_if #(
		.REG_ADDRESS(9),
		.REG_WIDTH  (4)
	) reg9_manual_control (
		.clk(s00_axi_aclk)
	);


// Instantiation of Axi Bus Interface S00_AXI
	StepperControllerNg_v1_0_S00_AXI # ( 
		.C_S_AXI_DATA_WIDTH(C_S00_AXI_DATA_WIDTH),
		.C_S_AXI_ADDR_WIDTH(C_S00_AXI_ADDR_WIDTH)
	) StepperControllerNg_v1_0_S00_AXI_inst (
		.S_AXI_ACLK(s00_axi_aclk),
		.S_AXI_ARESETN(s00_axi_aresetn),
		.S_AXI_AWADDR(s00_axi_awaddr),
		.S_AXI_AWPROT(s00_axi_awprot),
		.S_AXI_AWVALID(s00_axi_awvalid),
		.S_AXI_AWREADY(s00_axi_awready),
		.S_AXI_WDATA(s00_axi_wdata),
		.S_AXI_WSTRB(s00_axi_wstrb),
		.S_AXI_WVALID(s00_axi_wvalid),
		.S_AXI_WREADY(s00_axi_wready),
		.S_AXI_BRESP(s00_axi_bresp),
		.S_AXI_BVALID(s00_axi_bvalid),
		.S_AXI_BREADY(s00_axi_bready),
		.S_AXI_ARADDR(s00_axi_araddr),
		.S_AXI_ARPROT(s00_axi_arprot),
		.S_AXI_ARVALID(s00_axi_arvalid),
		.S_AXI_ARREADY(s00_axi_arready),
		.S_AXI_RDATA(s00_axi_rdata),
		.S_AXI_RRESP(s00_axi_rresp),
		.S_AXI_RVALID(s00_axi_rvalid),
		.S_AXI_RREADY(s00_axi_rready),
		
		.reg0_accel_deccel_if(reg0_accel_deccel),
		.reg1_accel_time_if  (reg1_accel_time),
		.reg2_const_time_if  (reg2_const_time),
		.reg3_deccel_time_if (reg3_deccel_time),
		.reg4_stepdir_if     (reg4_stepdir),
		.reg5_cis_steps_if   (reg5_cis_steps),
		.reg6_control_if     (reg6_control),
        .reg7_full_step_cnt  (reg7_full_step_cnt),
        .reg8_manual_mode_if (reg8_manual_mode),
        .reg9_manual_control_if(reg9_manual_control)
	);

	// Add user logic here
	
    StepperDriver # (
		.C_POSITION_WIDTH     (C_POSITION_WIDTH),
		.C_SPEED_WIDTH        (C_SPEED_WIDTH),
        .C_PULSE_TIME         (C_PULSE_TIME),
        .C_FULL_STEP_CNT_WIDTH(C_S00_AXI_DATA_WIDTH),
        .C_STEPS_PER_LINE_WIDTH(C_STEPS_PER_LINE_WIDTH),
        .C_ACCELERATION_WIDTH(C_ACCELERATION_WIDTH),
        .C_TIME_WIDTH(C_TIME_WIDTH),
        .C_PULSE_TIME_WIDTH(C_PULSE_TIME_WIDTH)
	) StepperDriver_inst (
		.aclk             (s00_axi_aclk),
		.aresetn          (s00_axi_aresetn),

		.reg0_accel_deccel_if (reg0_accel_deccel),
		.reg1_accel_time_if   (reg1_accel_time),
		.reg2_const_time_if   (reg2_const_time),
		.reg3_deccel_time_if  (reg3_deccel_time),
		.reg4_stepdir_if      (reg4_stepdir),
		.reg5_cis_steps_if    (reg5_cis_steps),
		.reg6_control_if      (reg6_control),
		.reg8_manual_mode_if  (reg8_manual_mode),
		.reg9_manual_control_if(reg9_manual_control),
		
        .STEP             (STEP),
        .DIR              (DIR),
        .BUSY             (BUSY),
        .SCAN_LINE        (SCAN_LINE),
        .PRINT_LINE       (PRINT_LINE),

        .FULL_STEP_COUNTER(reg7_full_step_cnt),
        
        .st_acc           (st_acc),
        .st_dec           (st_dec),
        .st_const         (st_const)
	);
	// User logic ends

	endmodule
